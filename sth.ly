\version "2.22.0"
\header {
  title = "sth"
  %composer = "Vojtěch Pachol"
  tagline = ""
}


\score {
  <<
    \new ChordNames {% \with{alignAboveContext = "staff" } {
      \set chordChanges = ##t
      %\set chordNameLowercaseMinor = ##t
      \germanChords

      \chordmode {
	c2 c des c4 g4
      }
    }

    \relative c' {
      \time 2/4
      \key c \major

      g'8 e' r4 r d8 c as f' r4 e d
    }
  >>
  \layout {}
  \midi {
    \tempo 4 = 80
  }
}
