\version "2.18.2"
\header {
  title = "Kukačka kuká"
  %composer = "Vojtěch Pachol"
  tagline = ""
}

melody = \relative c' {
  \time 3/4
  \tempo 4=170
  \key e \minor

  e4 e e b' b2 | a4 g8( fis) g4 a4 a2 | g4 fis e | fis2 fis4 e2. |
  e4 e e b' b2 | a4 g8( fis) g4 a4 a2 | g4 fis e | fis g a g fis e fis2 fis4 e2.
}


middle_harmony = \chordmode {
  \mark \markup \circle "2"
  e2:m e4:m e2:m e4:m d2 d4 d2 d4 c2 c4 b2 b4 e2:m e4:m e2:m e4:m 
  g2 g4 d2 d4 b2 b4 e2:m e4:m b2 b4 c2 a4:m b2 b4 e2:m r4 %\bar "||"

  \mark \markup \circle "3"
  \key e \major
  \repeat volta 2 { e2. e e4 e8 e e4 e e8 e e4 e2. e4 b2 e4 e8 e e4 | }
  \alternative { { e4 e8 e8 e4 | } { \key e \minor r2. | } }
  
  \mark \markup \circle "4"
  e2:m e4:m b2 b4 d2 d4 a2 a4 c2 a4:m b2 b4 e2:m e4:m e2:m d4
  g2 g4 d2 d4 b2 b4 e2:m e4:m b2 b4 c2 a4:m b2 b4 e2:m d4
}

\score {
  \new Staff = "staff" \relative c'
  {
	\time 3/4
	\tempo 4=170
	\key e \minor

	% 1
	\mark \markup \circle "1"
	e2._pizz b d a | e'4_arco b e c b2 |
	<e g,>4-- <e g,>8-- <e g,>-- <e g,>4-- | e2 e8( fis) g( d) g( a) b4 |
	a4 g8( fis) g4 fis8( dis) b( dis) fis4 | e4 e8( dis) e4 |
	fis8( dis) fis( g) fis4 | e8( c) e( c) e( c) dis2. |
	<e g,>4-- <e g,>8-- <e g,>-- <e g,>4-- \bar "||"
	<e g,>8-- <e g,>8-- <e g,>4-- <e g,>8-- <e g,>8--
	<e g,>4-- <e g,>8-- <e g,>-- <e g,>4--
	<e g,>8-.\pp\< <e g,>-. <e g,>-. <e g,>-. <e g,>-. <e g,>-.\! \bar"||"

	% 2,3,4
	<<
	  \new ChordNames \with{alignAboveContext = "staff" } {
		\set chordChanges = ##t
		\set chordNameLowercaseMinor = ##t
		\germanChords
		\middle_harmony
	  }
	  \middle_harmony

	  \new NullVoice = melody_voice { 
		\melody 
		\repeat volta 2 { R2.*7 }
		\alternative { { R2. } { R2. } }
		\melody
	  }
	  \new Lyrics \lyricsto melody_voice {
		Ku -- kač -- ka ku -- ká, ho -- lou -- bek vr -- ká
		na ze -- le -- nej vol -- ši,
		což -- pak si mys -- líš, můj znej -- mi -- lej -- ší,
		že jsem já nej -- hor -- ší, že jsem já nej -- hor -- ší.

		Ach, nej -- sem, nej -- sem, šá -- te -- ček dej sem,
		já ti ho vy -- pe -- ru,
		a -- bys měl bí -- lej, až půj -- deš k_ji -- nej
		v_so -- bo -- tu kve -- če -- ru, v_so -- bo -- tu kve -- če-ru.
	  }
	>>

	% 5
	\mark \markup \circle "5"
	\repeat volta 2 {
	  g4 b8( g) b4 a8 g fis4 d g b8( g) b4 a8 b c b a fis |
	  e4 g8( e) g4 dis8 e fis4 b e, g8( e) g4 dis8 e fis dis e fis |
	}
	e2.-\pp\< r4\! e_"pizz" r \bar "||"
  }

  \layout {}
  \midi {}
}
