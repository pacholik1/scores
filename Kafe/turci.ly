\version "2.18.2"
\header {
  title = "Turci"
  %composer = "Vojtěch Pachol"
  tagline = ""
}


\score {
  <<
	\new ChordNames {% \with{alignAboveContext = "staff" } {
	  \set chordChanges = ##t
	  %\set chordNameLowercaseMinor = ##t
	  \germanChords

	  \chordmode {
		\repeat volta 2 {
		  d2:m d:m d:m d:m 
		}
		d:m es:dim g:m e:dim
		g:m bes b:dim bes
		b:dim bes d:m gis:dim
		es bes:dim d:m d:m
	  }
	}

	\relative c' {
	  \time 2/4
	  \key d \minor

	  \repeat volta 2 {
		r8 <d a'> <d a'> r r <d a'> <d a'> r r <d a'> <d a'> r r <d a'> <d a'> r 
	  }
	  r <d a'> <d a'> r r <es a> <es a> r r <bes g'> <bes g'> r r <e bes'> <e bes'> r
	  r <bes g'> <bes g'> r r <bes f'> <bes f'> r r <b f'> <b f'> r r <bes f'> <bes f'> r
	  r <b f'> <b f'> r r <bes f'> <bes f'> r r <a f'> <a f'> r r <gis f'> <gis f'> r
	  r <g es'> <g es'> r r <bes e> <bes e> r r <a d> <a d> r r <a d> <a d> r

	}
  >>
  \layout {}
  \midi {
	\tempo 4 = 80
  }
}
